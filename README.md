# OpenAutoScope

This repository contains details on how to build a low-magnification (4x or 10x) brightfield/epifluorescence microscope with
a computer-controlled stage for a reasonably low cost ($3000 USD).

It contains:

1. Design files for a printed circuit board (PCB) that must be fabricated and assembled. This PCB controls the stage motors, the brightfield light source, and the epifluorescence light source.
2. Code to run on the microcontroller (Teensy 3.2) that controls that PCB.
3. Labview code to interface with the PCB.
4. A complete parts list to build the openAutoScope
5. A step-by-step tutorial showing how to assemble the openAutoScope.

![Image of fully-assembled openAutoScope](openAutoScopePicture.png)

openAutoScope was developed by Nate Cermak in [Steve Flavell's lab](http://flavell.mit.edu/) at MIT in 2017. It takes inspiration from many previous designs including [wormTracker 2.0](http://www.mrc-lmb.cam.ac.uk/wormtracker/index.php?action=hardware). Others who have greatly contributed to the design, instructions, and troubleshooting details found here are Philip Kidd, Stephanie Yu, and Andrew Bahle.

## Getting started
To get started, take a look at the construction tutorial and the parts list.

## Specifications
- Brightfield field of view is 1.84 x 1.47mm with 4X objective (0.74 x 0.59 mm with 10X objective)
- Image resolution 1280x1021 pixels.
- Up to 150 frames per second
- 5mW 532nm laser illumination over approximately 0.9mm diameter circle in sample plane.
- Microscope moves in X and Y, while stage only moves in Z (vertically).
- Typical stage speed 1.25mm/s.
- Stage movement resolution is 1.25 um.
- Travel range is approximately 115 mm in each dimension.
- Stage movement, laser and LED are all software-controllable.
