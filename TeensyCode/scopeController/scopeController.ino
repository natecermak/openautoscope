#include <AccelStepper.h>


#define N_RESET_SLEEP 2
#define MS3 3
#define MS2 4
#define MS1 5
#define N_ENABLE 6
#define LED 21
#define LASER 23

AccelStepper stepperX(AccelStepper::DRIVER,1,0);
AccelStepper stepperY(AccelStepper::DRIVER,8,7);
AccelStepper stepperZ(AccelStepper::DRIVER,15,14);


long xmax, ymax, zmax;
long xmin, ymin, zmin;
long x,y,z;
bool motorsEnabled;
char *comBuf;
int nChar;
unsigned long lastBroadcastTime, broadcastInterval;

void setStepMode(int axis, int mode){
  // 0 = FULL, 1 = Half, 2 = 1/4, 3=1/8, 4=1/16, 5=1/32
  if (mode == 0){ //Full step
    digitalWrite(MS1 + 7*axis, 0); 
    digitalWrite(MS2 + 7*axis, 0); 
    digitalWrite(MS3 + 7*axis, 0); 
  } 
  if (mode == 1){ // 1/2 step 
    digitalWrite(MS1 + 7*axis, 1); 
    digitalWrite(MS2 + 7*axis, 0); 
    digitalWrite(MS3 + 7*axis, 0); 
  }
  if (mode == 2){ // 1/4 step
    digitalWrite(MS1 + 7*axis, 0); 
    digitalWrite(MS2 + 7*axis, 1); 
    digitalWrite(MS3 + 7*axis, 0); 
  }
  if (mode == 3){ // 1/8 step
    digitalWrite(MS1 + 7*axis, 1); 
    digitalWrite(MS2 + 7*axis, 1); 
    digitalWrite(MS3 + 7*axis, 0); 
  }
  if (mode == 4){ // 1/16 step
    digitalWrite(MS1 + 7*axis, 0); 
    digitalWrite(MS2 + 7*axis, 0); 
    digitalWrite(MS3 + 7*axis, 1); 
  }
  if (mode == 5){
    digitalWrite(MS1 + 7*axis, 1); 
    digitalWrite(MS2 + 7*axis, 1); 
    digitalWrite(MS3 + 7*axis, 1); 
  }
}


void setup() {
  Serial.begin(115200); // baud is ignored on teensy.
  Serial.println("Loading LIBController code!");
  for (int j=0; j<25; j++)
    pinMode(j, OUTPUT);

  stepperX.setMaxSpeed(1000.0);
  stepperY.setMaxSpeed(1000.0);
  stepperZ.setMaxSpeed(1000.0);
  stepperX.setAcceleration(50000.0); // 20ms to get up to max speed
  stepperY.setAcceleration(50000.0);
  stepperZ.setAcceleration(5000.0);

  xmin = ymin = zmin = -2000000000;
  xmax = ymax = zmax = 2000000000;
  
  setStepMode(0,5);
  setStepMode(1,5);
  setStepMode(2,5);
  
  comBuf = (char*) malloc(24);
  nChar=0;

  broadcastInterval = 5;
  motorsEnabled=false;

  lastBroadcastTime=millis();
}




void loop() {
  
  // print the current position periodically!
  if(millis()-lastBroadcastTime > broadcastInterval){ 
      x=stepperX.currentPosition();
      y=stepperY.currentPosition();
      z=stepperZ.currentPosition();
    Serial.print("[");
    Serial.print(x);
    Serial.print(" ");
    Serial.print(y);
    Serial.print(" ");
    Serial.print(z);
    Serial.print("]\n");
    Serial.send_now(); // make sure anything in the buffer gets sent! 
    lastBroadcastTime += broadcastInterval;
  }
  
  if (Serial.available()>0){
    Serial.readBytes(comBuf+nChar, 1); // read one byte into the buffer
    nChar++; // keep track of the number of characters we've read!
        
    if (comBuf[nChar-1] == '\n'){ // termination character for string - means we've recvd a full command!
      if      (comBuf[0] == 'l')    digitalWrite(LED, LOW);
      else if (comBuf[0] == 'L')    digitalWrite(LED, HIGH);
      else if (comBuf[0] == 'k')    digitalWrite(LASER, LOW);
      else if (comBuf[0] == 'K')    digitalWrite(LASER, HIGH);
      else if (comBuf[0] =='X' && motorsEnabled){
        int targetPos = atoi(comBuf+1);
        if (targetPos>xmax) targetPos=xmax;
        if (targetPos<xmin) targetPos=xmin;
        stepperX.moveTo(targetPos);
      }
      else if (comBuf[0] =='Y' && motorsEnabled){
        int targetPos = atoi(comBuf+1);
        if (targetPos>ymax) targetPos=ymax;
        if (targetPos<ymin) targetPos=ymin;
        stepperY.moveTo(targetPos);
      }
      else if (comBuf[0] =='Z' && motorsEnabled){
        int targetPos = atoi(comBuf+1);
        if (targetPos>zmax) targetPos=zmax;
        if (targetPos<zmin) targetPos=zmin;
        stepperZ.moveTo(targetPos);
      }
      else if (comBuf[0] == 'E'){
        motorsEnabled = true;
        for (int j=0; j<3; j++){
          digitalWrite(N_RESET_SLEEP +7*j,HIGH);
          digitalWrite(N_ENABLE +7*j,LOW);
        }      
      }
      else if (comBuf[0] == 'e'){
        motorsEnabled = false;
        for (int j=0; j<3; j++){
          digitalWrite(N_RESET_SLEEP +7*j,LOW);
          digitalWrite(N_ENABLE +7*j,HIGH);
        }
      }
      else if (comBuf[0] =='S'){ // sets the speed
        if (comBuf[1] =='X') stepperX.setMaxSpeed(atof(comBuf+2));
        if (comBuf[1] =='Y') stepperY.setMaxSpeed(atof(comBuf+2));
        if (comBuf[1] =='Z') stepperZ.setMaxSpeed(atof(comBuf+2));
      }
      else if (comBuf[0] =='A'){ // sets the acceleration
        if (comBuf[1] =='X') stepperX.setAcceleration(atof(comBuf+2));
        if (comBuf[1] =='Y') stepperY.setAcceleration(atof(comBuf+2));
        if (comBuf[1] =='Z') stepperZ.setAcceleration(atof(comBuf+2));
      }
      else if (comBuf[0] =='M'){ // sets the microstepping mode
        int microSteppingMode = atoi(comBuf+2);
        if (microSteppingMode >= 0 && microSteppingMode <= 5){
          if (comBuf[1] =='X') setStepMode(0, microSteppingMode);
          if (comBuf[1] =='Y') setStepMode(1, microSteppingMode);
          if (comBuf[1] =='Z') setStepMode(2, microSteppingMode);
        }
      }
      else if (comBuf[0] =='R'){ // sets minimum of range
        if      (comBuf[1] =='X') xmax=atoi(comBuf+2);
        else if (comBuf[1] =='Y') ymax=atoi(comBuf+2);
        else if (comBuf[1] =='Z') zmax=atoi(comBuf+2);
      }
      else if (comBuf[0] =='r'){ // sets minimum of range
        if      (comBuf[1] =='X') xmin=atoi(comBuf+2);
        else if (comBuf[1] =='Y') ymin=atoi(comBuf+2);
        else if (comBuf[1] =='Z') zmin=atoi(comBuf+2);
      }
      nChar = 0; // reset the pointer!
    }
  }
  stepperX.run();
  stepperY.run();
  stepperZ.run();
}
